exec { "apt-get update":
  path => "/usr/bin",
}

include nginx
include python
include git
include supervisor
include requirements
