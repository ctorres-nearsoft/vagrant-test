# Class: python
#
#
class python {
    
    package { 'python3':
        ensure => installed,
        require => Exec["apt-get update"],
    }

    package { 'python3-pip':
        ensure => installed,
        require => Exec["apt-get update"],
    }

}