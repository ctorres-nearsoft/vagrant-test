#!/bin/bash

NAME="prueba"                                  # Name of the application
DJANGODIR=/vagrant/prueba/                         # Django project directory
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=prueba.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=prueba.wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE} --workers $NUM_WORKERS --bind 0.0.0.0:8000
