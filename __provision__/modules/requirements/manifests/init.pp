# Class: requirements
#
#
class requirements {

    require python

    exec { 'requirements':
        command => 'pip3 install -r /vagrant/__provision__/modules/requirements/files/requirements.pip',
        path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    }

}